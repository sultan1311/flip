<?php
    require_once 'config/Database.php';
    
    class Model
    {
        public function __construct()
        {
            $this->db = Database::get_instance();   
        }   

        function import($files)
        {
            // Check file is exists
            if (file_exists($files)) {
                // Start import
                $sql = file_get_contents($files);
                $import = $this->db->exec($sql);

                if ($import !== FALSE) {
                    echo "Database import successful";
                }else{
                    echo "Database import failed";
                }
            }else{
                echo "File not found";
            }
        }

        public function save($data)
        {
            // Store to mysql format
            $dataInsert = '("'.$data->id.'", "'.$data->amount.'", "'.$data->status.'", "'.$data->timestamp.'", "'.$data->bank_code.'", "'.$data->account_number.'", "'.$data->beneficiary_name.'", "'.$data->remark.'", "'.$data->receipt.'", "'.$data->time_served.'", "'.$data->fee.'")';

            // Insert data
            $sql = $this->db->prepare("REPLACE INTO disburse VALUES $dataInsert");

            if ($sql->execute() !== FALSE) {
                $result = "Request API Success";
            }else{
                $result = "Request API Failed";
            }

            echo $result;
        }

        public function getLast()
        {
            $sql = $this->db->prepare("SELECT id FROM disburse LIMIT 1");
            $sql->execute();

            return $sql;
        }
    }

?>
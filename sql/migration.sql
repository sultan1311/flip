CREATE DATABASE IF NOT EXISTS `flip` ;
USE `flip`;

CREATE TABLE IF NOT EXISTS `disburse` (
    `id` BIGINT(11) NOT NULL,
    `amount` DOUBLE NOT NULL,
    `status` VARCHAR(25) NOT NULL,
    `timestamp` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `bank_code` VARCHAR(10) NOT NULL,
    `account_number` VARCHAR(15) NOT NULL,
    `beneficiary_name` VARCHAR(100) NOT NULL,
    `remark` TEXT DEFAULT NULL,
    `receipt` TEXT DEFAULT NULL,
    `time_served` DATETIME DEFAULT NULL,
    `fee` DOUBLE NOT NULL,
    PRIMARY KEY (`id`)
);

<?php
    class Database
    {
        // Database Configuration
        private static $hostname = "localhost";
        private static $username = "";
        private static $password = "";
        private static $database = "flip";
        

        public function __construct()
        {   
        }        

        public static function get_instance()
        {
            // Connect to Database
            try {
                $conn = new PDO("mysql:host=".self::$hostname.";dbname=".self::$database."", self::$username, self::$password);
                // set the PDO error mode to exception
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);                    
                return $conn;
            } catch (PDOException $err) {
                echo "Connection failed: " . $err->getMessage();
            }            
        }
    }
    
?>

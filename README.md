# Tata Cara Penggunaan
1. Cloning git repository dengan cara : git clone https://sultan1311@bitbucket.org/sultan1311/flip.git
2. Buat database dengan nama : flip
3. Masuk ke folder config, edit file Database.php untuk mengatur hostname, username dan password MySQL
4. Kembali ke root repository folder
5. Import table dengan perintah : php Migration.php
6. Lakukan post request api dengan perintah : php PostApplicant.php
7. Lakukan get request api dengan perintah : php GetApplicant.php

<?php
    require_once 'Request.php';
    require_once 'models/Model.php';

    class Post
    {
        public function __construct()
        {
            // Import request class
            $this->request = new Request();

            // Import model class
            $this->model = new Model();

            $this->index();
        }

        public function index()
        {
            // Define header for curl
            $header = array(
                "Content-Type: application/x-www-form-urlencoded",
            );

            // Define data post
            $data = array(
                'bank_code' => 'bsm',
                'account_number' => '1324324525',
                'amount' => '1000000',
                'remark' => 'test api'
            );
    
            // Define url for curl
            $url = 'https://nextar.flip.id/disburse';
    
            $result = $this->request->post($url,$header,$data);
            $result = json_decode($result);           

            return $this->model->save($result);
        }
    }    
    
?>
<?php
    require_once 'Request.php';
    require_once 'models/Model.php';

    class Get
    {
        public function __construct()
        {
            // Import request class
            $this->request = new Request();

            // Import model class
            $this->model = new Model();

            $this->index();
        }

        public function index()
        {
            // Define header for curl
            $header = array(
                "Content-Type: application/x-www-form-urlencoded",
            );
    
            // Get last id of disbursement
            $lastId = @$this->model->getLast()->fetch(PDO::FETCH_ASSOC)['id'];

            // Define url for curl
            $url = 'https://nextar.flip.id/disburse/'.$lastId;
    
            $result = $this->request->get($url,$header);
            $result = json_decode($result);           
            
            return $this->model->save($result);
        }
    }    
?>
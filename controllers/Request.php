<?php
    class Request
    {
        private static $secretKey = "HyzioY7LP6ZoO7nTYKbG8O4ISkyWnX1JvAEVAhtWKZumooCzqp41";
                
        public static function get($url,$header)
        {
            // Start curl
            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
    
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

            curl_setopt($ch, CURLOPT_USERPWD, self::$secretKey.":");
    
            $response = curl_exec($ch);
            curl_close($ch);

            // End
    
            return $response;
    
        }

        public static function post($url,$header,$data = false)
        {
            // Start curl
            $ch = curl_init();
        
            if ($data !== FALSE) {
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            }
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
    
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

            curl_setopt($ch, CURLOPT_USERPWD, self::$secretKey.":");
    
            $response = curl_exec($ch);
            curl_close($ch);

            // End
    
            return $response;
    
        }        
    }
    
?>